
(function($) {
        $(window).on("load", function() {
            $(".ulScroll").mCustomScrollbar({
                axis: "y",
                theme: "dark-3"
            });
			$(".runBottomRightOneScroll").mCustomScrollbar({
                axis: "x",
                theme: "dark-3"
            }); 
			$(".tableBody").mCustomScrollbar({
			  axis: "y",
			  theme: "dark-3"
			});
			$(".scrollSec").mCustomScrollbar({
			  axis: "y",
			  theme: "dark-3"
			});
			
			$(".scrollsetstart").mCustomScrollbar({
			  axis: "y",
			  theme: "dark-3"
			});
			 $(".scrollsetstart").mCustomScrollbar({
			  axis: "y",
			  theme: "dark-3"
			});
			//Four Scroll
			$(".rpFourBox").mCustomScrollbar({
			  axis: "x",
			  theme: "dark-3"
			});
			//Checkbox select all functions
			$().chkAll({
				mainClass:'.chkAll', 
				subClass:'.chk',    
				containerClass:".chkgrop"	
		
			});
        });
    })(jQuery);

//popup Scripts
	$('.runHead button').click(function(e) {
        $('.miniRpFour').toggle();
		$(this).toggleClass('btnClass');
		$('.miniRpTwo, .statusPopOne, .MapPop, .miniRpThree, .miniRpOne').hide();
		$('.btnSecondClick, .btnFirstClick').removeClass('btnGreen');
    });
	$('.btnSecondClick').click(function(e) {
        $('.miniRpTwo').toggle();
		$(this).toggleClass('btnGreen');
		$('.btnFirstClick').removeClass('btnGreen');
		$('.miniRpFour, .statusPopOne, .MapPop, .miniRpThree, .miniRpOne').hide();
    });
	$('.btnFirstClick').click(function(e) {
        $('.statusPopOne').toggle();
		$(this).toggleClass('btnGreen');
		$('.btnSecondClick').removeClass('btnGreen');
		$('.miniRpFour, .miniRpTwo, .MapPop, .miniRpThree, .miniRpOne').hide();
    });
	$('.btnExtra').click(function(e) {
        $('.miniRpThree').toggle();
		$('.miniRpFour, .miniRpTwo, .MapPop, .statusPopOne, .miniRpOne').hide();
		$('.btnSecondClick, .btnFirstClick').removeClass('btnGreen');
    });
	$('.runRepeat .timeLink a').click(function(e) {
        $('.MapPop').toggle();
		$('.mainOne').toggle();
		$('.miniRpFour, .miniRpTwo, .miniRpThree, .statusPopOne, .miniRpOne').hide();
		$('.btnSecondClick, .btnFirstClick').removeClass('btnGreen');
    });
	$('li.blueText').click(function(e) {
		$('.firstSet li').eq(1).addClass('active').siblings().removeClass('active');
		$('.mainBox h1').text('发送消息');
		$('.mainTwo').toggle();
		$('.mainOne').hide();
		$('.btnSecondClick, .btnFirstClick').removeClass('btnGreen');
    });
	$('.markBtn').click(function(e) {
		$('.firstSet li').eq(2).addClass('active').siblings().removeClass('active');
		$('.mainBox h1').text('新增发车');
		$('.splSet').toggle();
		$('.mainTwo').hide();
		$('.btnSecondClick, .btnFirstClick').removeClass('btnGreen');
    });
	$('.btnBox .lastBtn').click(function(e) {
        $('.rpOne, .rpWrapper').show();
    });
	$(".firstUlBox ul li").eq(1).click(function() {
		$('.rpTwo, .rpWrapper').show();
    });
	$(".selectBox h1").click(function() {
		$('.miniRpOne').toggle();
		$('.miniRpFour, .miniRpTwo, .miniRpThree, .statusPopOne, .MapPop').hide();
		$('.btnSecondClick, .btnFirstClick').removeClass('btnGreen');
    });
	
	
	$('.miniRpFour .redCross').click(function(e) {
        $('.miniRpFour').hide();
		$('.runHead button').removeClass('btnClass');
    });
	$('.miniRpTwo .redCross').click(function(e) {
        $('.miniRpTwo').hide();
		$('.btnSecondClick').removeClass('btnGreen');
    });
	$('.statusPopOne .close').click(function(e) {
        $('.statusPopOne').hide();
		$('.btnFirstClick').removeClass('btnGreen');
    });
	$('.miniRpThree .circleArrow').click(function(e) {
		$('.miniRpThree').css('display', 'none');
	});
	$('.MapPop .close, .splSet .myBtnSet .nxtBtn').click(function(e) {
        $('.MapPop').hide();
    });
	$('.rpOne .popcross').click(function(e) {
        $('.rpOne, .rpWrapper').hide();
    });
	$('.rpTwo .cross').click(function(e) {
        $('.rpTwo, .rpWrapper').hide();
    });
	$(".miniRpOne .redCross").click(function() {
		$('.miniRpOne').hide();
    });
	$().windowClick({
		popup:'.rpWrapper',
	});