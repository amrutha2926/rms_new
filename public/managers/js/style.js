	// JavaScript Document
(function($){
	//plugin for chkbox
	"use strict";
	$.fn.chkAll=function(options){
		var settings=$.extend({
			containerClass:null,
			mainClass:null,
			subClass:null,
		     },options);
			 
		
		//function for select all
		var cont=settings.containerClass;
		$(cont).find(settings.mainClass).change(function() {
            var status = this.checked;
			$(cont).find(settings.subClass).each(function(){ 
				this.checked = status; 
				$(cont).find(settings.mainClass).each(function(){ 
						  this.checked = status; 
						});
			});
        });
		
		$(cont).find(settings.subClass).change(function(){ 
			//Uncheck if any of the child's status is false
			if(this.checked === false){
				$(cont).find(settings.mainClass).each(function(){ 
						this.checked = false; 
						});
			}
			
			//calculating TOTAL checked CHECKBOX
			var chk_num=$(cont).find(settings.subClass);
			var b=0;
			for (var i=0; i<chk_num.length; i++) {   
			   if (chk_num[i].type === "checkbox" && chk_num[i].checked === true){
				  b++;
				  if (b === $(cont).find(settings.subClass).length ){ 
						$(cont).find(settings.mainClass).each(function(){ 
						  this.checked = true; 
						});
					}
			   }
			}
			});
		return this;	
	};
	
	
	
}( jQuery ));



//Jayu Demo Popup simple-jquery
(function($) {
	$.fn.simplePopup = function() {
  
	  var simplePopup = {
  
		// Обработчики
		initialize: function(self) {
  
		  var popup = $(".js__popup");
		  var body = $(".js__p_body");
		  var close = $(".js__p_close");
		  var hash = "#popup";
  
		  var string = self[0].className;
		  var name = string.replace("js__p_", "");
  
		  // Переопределим переменные, если есть дополнительный попап
		  if ( !(name === "start") ) {
			name = name.replace("_start", "_popup");
			popup = $(".js__" + name);
			name = name.replace("_", "-");
			hash = "#" + name;
		  };
  
		  // Вызов при клике
		  self.on("click", function() {
			simplePopup.show(popup, body, hash);
			return false;
		  });
  
		  $(window).on("load", function() {
			simplePopup.hash(popup, body, hash);
		  });
  
		  // Закрытие
		  body.on("click", function() {
			simplePopup.hide(popup, body);
		  });
  
		  close.on("click", function() {
			simplePopup.hide(popup, body);
			return false;
		  });
  
		  // Закрытие по кнопке esc
		  $(window).keyup(function(e) {
			if (e.keyCode === 27) {
			  simplePopup.hide(popup, body);
			}
		  });
  
		},
  
		// Метод центрирования
		centering: function(self) {
		  var marginLeft = -self.width()/2;
		  return self.css("margin-left", marginLeft);
		},
  
		// Общая функция показа
		show: function(popup, body, hash) {
		  simplePopup.centering(popup);
		  body.removeClass("js__fadeout");
		  popup.removeClass("js__slide_top");
		  window.location.hash = hash;
		},
  
		// Общая функция скрытия
		hide: function(popup, body) {
		  popup.addClass("js__slide_top");
		  body.addClass("js__fadeout");
		  window.location.hash = "#";
		},
  
		// Мониторим хэш в урле
		hash: function(popup, body, hash) {
		  if (window.location.hash === hash) {
			simplePopup.show(popup, body, hash);
		  }
		}
  
	  };
  
	  // Циклом ищем что вызвано
	  return this.each(function() {
		var self = $(this);
		simplePopup.initialize(self);
	  });
  
	};
  })(jQuery);