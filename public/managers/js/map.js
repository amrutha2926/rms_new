// JavaScript Document
(function($) {
            $(window).on("load", function() {
                $(".scroll").mCustomScrollbar({
                    axis: "y",
                    theme: "dark-3"
                });
                $(".srollTwo").mCustomScrollbar({
                    axis: "y",
                    theme: "dark-3"
                });
                $(".tableScroll").mCustomScrollbar({
                    axis: "y",
                    theme: "dark-3"
                });

            });

        })(jQuery);
        $(document).ready(function(e) {

            $().chkAll({
                mainClass: '.chkAll',
                subClass: '.chk',
                containerClass: ".chkgrop"

            });

            $(".statusPopOne .markBtnOne").click(function() {
                $(".statusPopOne").hide();
                $(".btn").removeClass("grnBtn");
            });
            $(".btn").click(function() {
                $(this).toggleClass("grnBtn");
                $(".statusPopOne").show();
            });



        });
        var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1", {
            defaultTab: 0
        });
        var TabbedPanels2 = new Spry.Widget.TabbedPanels("TabbedPanels2", {
            defaultTab: 0
        });
        var TabbedPanels3 = new Spry.Widget.TabbedPanels("TabbedPanels3", {
            defaultTab: 0
        });
        var TabbedPanels4 = new Spry.Widget.TabbedPanels("TabbedPanels4", {
            defaultTab: 0
        });
		 $(document).ready(function(e) {
            $('.blueText').click(function(e) {
                $('.mainOne').hide();
                $('.mainTwo').show();
                $('.firstSet li').eq(1).addClass('active').siblings().removeClass('active');
                $('.firstSet li').eq(1).parents(".firstSetMain").find("h1").text("发送消息");
            });

            $('.markBtn').click(function(e) {
                $('.mainTwo').hide();
                $('.mainThree').show();
                $('.firstSet li').eq(2).addClass('active').siblings().removeClass('active');
                //$('.firstSet li').eq(1).addClass('active').siblings().removeClass('active');
                $('.firstSet li').eq(2).parents(".firstSetMain").find("h1").text("轨迹回放");

                //script for timer
                var counter = 5;
                var interval = setInterval(function() {
                    counter--;
                    $('.fivz').text(counter);
                    if (counter == 0) {
                        $('.firstSet li').eq(3).addClass('active').siblings().removeClass('active');
                        $('.firstSet li').eq(3).parents(".firstSetMain").find("h1").text("查看视频");
                        $('.mainThree').hide();
                        $('.mainFour').show();
                        clearInterval(interval);
                    }
                }, 1000);
            });
            $('.close').click(function(e) {
                $('.MapPop').fadeOut();
            });
            $('.closez').click(function(e) {
                $('.mainRightMap').fadeOut();
				$('.lastButton').removeClass('grnBtn');
            });

            //Menu click function
            $('.rightmap li').click(function(e) {
                $(this).children('a').css({'color': '#0c0c0c',"font-weight":"bold"});
                $(this).siblings().children('a').css({'color': '#747474',"font-weight":"normal"});
				$('.sixest').hide();
				$('.lastButton').removeClass('grnBtn');
                if ($(this).index() == 0) {
					$('.mainRightMap').show();
                    $('.fiveCntz').toggle();
                    $('.threeest,.fourest,.rightSet').hide();
                }
                if ($(this).index() == 1) {
					$('.mainRightMap').show();
                    $('.rightSet').toggle();
                    $('.threeest,.fourest,.fiveCntz').hide();
                }
                if ($(this).index() == 2) {
					$('.mainRightMap').show();
                    $('.threeest').toggle();
                    $('.rightSet,.fourest,.fiveCntz').hide();

                }
                if ($(this).index() == 3) {
					$('.mainRightMap').show();
                    $('.fourest').toggle();
                    $('.rightSet,.threeest,.fiveCntz').hide();
                }
                if ($(this).index() == 4) {
                    $('.mainRightMap').hide();
                }
                if ($(this).index() == 5) {
                   $('.mainRightMap').hide();
                }
                if ($(this).index() == 6) {
                    $('.mainRightMap').hide();
                }
                if ($(this).index() == 7) {
                   $('.mainRightMap').hide();
                }
            });
        });
        $('.lastClick').click(function(e) {
            $(this).toggleClass("grnBtn");
            $('.sixest').toggle();
			$('.rightSet,.threeest,.fiveCntz,.fourest').hide();
        });
		$('.greenBtn').click(function(e) {
            $('.threeest').hide();
			return false;
        });
        $(".eftBtn").click(function() {
            $(this).toggleClass("grnBtn");
            $(".sectionOne").slideToggle();
        });
        $(".close").click(function() {
            $(".popUpTwo").hide();
        });
        $(".containerHistory").click(function() {
            $(".popUpTwo").show();
        });

        $(".sliding").click(function() {
            $(".sectionOne").slideToggle();
            $(".eftBtn").toggleClass("grnBtn");
        });
        $(".containerMap").click(function() {
            $(".popUpTwo").css("display", "block");
        });
		
        (function($) {
            $(window).on("load", function() {
                $(".myScroll").mCustomScrollbar({
                    axis: "y",
                    theme: "dark-3"
                });
            });
        })(jQuery);
		
		$('.fourest input').change(function(e) {
			if(this.checked==true){
				$(this).parents('tr').css('background','#fff');
				$(this).parent().siblings().find('.tdText').show();
			}
			else{
				$(this).parents('tr').css('background','#f4f4f4');
				$(this).parent().siblings().find('.tdText').hide();
			}
        });
        $(window).click(function(e) {
            var target = $(event.target);
            if (target.is(".popUpTwo")) {
                target.hide();
            }
        });