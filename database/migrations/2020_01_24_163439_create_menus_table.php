<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('menu_id');
            $table->String('menu_name');
            $table->String('item_code');
            $table->String('name_in_bill');
            $table->String('kitchen_type');
            $table->String('category_name');
            $table->String('portion_name');
            $table->String('Description');
            $table->String('status')->nullable();
            $table->String('Adds_on')->nullable();
            $table->String('stock')->nullable();
            $table->String('Dynamic_rate')->nullable();
            $table->String('show_in_kot')->nullable();
            $table->String('print_kot')->nullable();
            $table->String('except_tax')->nullable();
            $table->String('Barcode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
