<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Table;
class tableController
{
    //
    public function add(Request $request)
  {
        Table::create($request->except('_token'));


        return redirect('/Table');

      }
      public function show()
      {
        $tables=Table::all();
        return view('table',compact('tables'));
      }


         public function edit(Request $request) {
           $tid=$request->table_id;
           $num= $request->table_no;
           $seats=$request->seats;
           $tables=Table::find($tid);
           $tables->update($request->except('_token'));

          return redirect('/Table');
         }
         public function destroy($id) {
            $tables=Table::find($id)->delete();
              return redirect('/Table');        
          //  echo "Record deleted successfully.<br/>";
          //  echo '<a href = "/reg_view">Click Here</a> to go back.';
         }
}
