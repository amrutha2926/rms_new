<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kitchen;
class kitchenController
{
  public function add(Request $request)
{
        // dd($request);
        Kitchen::create($request->except('_token'));

      return redirect('/Kitchen');
  }
  public function show()
  {
    $kitchens = Kitchen::all();
    // dd($kitchens);
    return view('kitchen_type',compact('kitchens'));
  }
  public function display($id) {
        $users = DB::select('select * from tbl_kitchen where id = ?',[$id]);
        return view('kitchen_update',['users'=>$users]);
     }
     public function edit(Request $request) {
       $kid=$request->kitchen_id;
        $k_name = $request->kitchen_type;
        $kitchen=Kitchen::find($kid);
      //  dd($kitchen);
      $kitchen->update($request->except('_token'));
      //  echo "Record updated successfully.<br/>";
      //  echo '<a href = "/kitchen_type">Click Here</a> to go back.';
      return redirect('/Kitchen');
     }
     public function destroy($id)
     {
         $kitchen=Kitchen::find($id)->delete();
         return redirect('/Kitchen');

     }
    //
}
