<?php

namespace App\Http\Controllers;
use App\Models\Table;
use App\Models\Menu;
use App\Models\Menurate;
use App\Models\OrderCart;

use DB;


use Illuminate\Http\Request;

class OrderController
{
  public function index()
  {
   //Menu::create($request->except('_token'));
   $tables = Table::with('seats')->get();
   return view('new_order',compact('tables'));
   //dd($tables);
 }
  public function tables_seat($table_id){
   $table=Table::with('seats')->where($table_id)->get();
   //return view('New_order',compact('table'));
  echo $tables;
  }

  public function get_products(Request $request)
  {
    $search_value = $request->keyword;

    $menu_items   = Menu::where('menu_name', 'like','%'.$search_value.'%')->get();

    echo "<ul id='search-list'>";
    foreach($menu_items as $items) 
    {
       echo "<li class='menu_item' id='$items[menu_id]'>$items[menu_name]</li>";
    } 
    echo "</ul>";

  }

  public function get_menu_items(Request $request)
  {
    $menu_id   = $request->menu_value;
    $tableno   = $request->table;  

    $rate_data = Menurate::where('menu_id', $menu_id)->first();

    // DB::table('rms_OrderCart')->insert(array(
    //   array('rate_id' => $rate_data->rate_id)));

   $this->insert_fn($rate_data, $tableno );

    echo "<tr>
    <td width='5%'>$rate_data->rate_id</td>
    <td width='20%'>$rate_data->menu_name</td>
    <td width='8%'>Qty <strong>1</strong></td>
    <td width='15%'>$rate_data->portion_name</td>
    <td width='17%'>Rs. <strong>$rate_data->rate</strong></td>
     
    <td id='hid_td' width='20%'><textarea placeholder='Enter notes'></textarea></td>
    <td id='hid_td' width='5%'><a href='#' class='editbt'><img src='order/images/edit.png' alt='' /></a></td>
    <td id='hid_td' width='5%'><a href='#' class='dltbt'><img src='order/images/dltico.png' alt='' /></a></td>
    <td id='hid_td' width='5%'><a href='#' class='donebt'><img src='order/images/tic.png' alt='' /></a></td>
     
    </tr>";
  }

  function insert_fn($rate_data, $tableno)
  {
    DB::table('order_carts')->insert(array(array('rate_id' => $rate_data->rate_id,'table_id' =>$tableno)));
  }

  public function place_order(Request $request)
  {
    $tblno = $request->tableno;

    $order_data = OrderCart::with(['manurate'])->where('table_id', $tblno)->get();

    foreach($order_data as $data)
    {

    echo "<tr>
    <td width='5%'>$data->order_id</td>
    <td width='50%'>$data->menurate->menu_name</td>
    <td width='20%'>$data->portion_name</td>
    <td width='25%' align='right'>Rs. <strong>$data->rate</strong></td>
    <td><input type='hidden' value='$data->rate' name='hid_rate' id='hid_rate'></td>
    </tr>"; 
    }   

  }
  public function check_order_exist()
  {
    $id = request()->id;
    $item['data'] = "";
    $order_data = OrderCart::with(['manurate'])->where('table_id', $id)->get();
    if($order_data){
      foreach ($order_data as $data) {
        $item['data'] = "<tr>
    <td width='5%'>1</td>
    <td width='20%'>name</td>
    <td width='8%'>Qty <strong>1</strong></td>
    <td width='15%'>once</td>
    <td width='17%'>Rs. <strong>23432</strong></td>     
    <td id='hid_td' width='20%'><textarea placeholder='Enter notes'></textarea></td>
    <td id='hid_td' width='5%'><a href='#' class='editbt'><img src='order/images/edit.png' alt='' /></a></td>
    <td id='hid_td' width='5%'><a href='#' class='dltbt'><img src='order/images/dltico.png' alt='' /></a></td>
    <td id='hid_td' width='5%'><a href='#' class='donebt'><img src='order/images/tic.png' alt='' /></a></td>
     
    </tr>";;
      }
    }else{
      $item['data'] = "";
    }
    echo($item['data']);
   

  }

}

