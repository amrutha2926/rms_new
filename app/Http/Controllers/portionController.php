<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Portion;
class portionController
{
  public function add(Request $request)
{
  Portion::create($request->except('_token'));


      return redirect('/Portion');

    }
    public function show()
    {
      $portions=Portion::all();
      return view('portion',compact('portions'));
    }

       public function edit(Request $request) {
          $pid = $request->portion_id;
          $pname=$request->portion_name;
          $portion=Portion::find($pid);
          $portion->update($request->except('_token'));
          return redirect('/Portion');
       }
       public function destroy($id) {
          $res=Portion::find($id)->delete();
          return redirect('/Portion');
        //  echo "Record deleted successfully.<br/>";
        //  echo '<a href = "/reg_view">Click Here</a> to go back.';
       }
    //
}
