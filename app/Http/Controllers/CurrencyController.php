<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Currency;

class CurrencyController extends Controller
{
  public function add(Request $request)
{
      Currency::create($request->except('_token'));


      return redirect('/Currency');

    }
    public function show()
    {
      $currencies=Currency::all();
      return view('currency',compact('currencies'));
    }
    public function display_cur($id) {
          $users = DB::select('select * from tbl_currency where id = ?',[$id]);
          return view('currency',['users'=>$users]);
       }
       public function edit(Request $request) {
         $cid=$request->currency_id;
          $c_name = $request->currency_name;
          $country=$request->country;
          $currency=Currency::find($cid);
        //  dd($kitchen);
        $currency->update($request->except('_token'));
        return redirect('/Currency');
       }
       public function destroy($id) {
         $res=Currency::find($id)->delete();
         return redirect('/Currency');
        //  echo "Record deleted successfully.<br/>";
        //  echo '<a href = "/reg_view">Click Here</a> to go back.';
       }
    //
}
