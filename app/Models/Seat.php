<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{
    protected $primaryKey = 'seat_id';
    protected $fillable = ['name','table_id','status'];
    public function tables(){
        return $this->hasOne('App\Models\Table','table_id','table_id');
    }

}
