<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menurate extends Model
{
  protected $primaryKey = 'rate_id';
protected $fillable = ['menu_id',
    'Mode_name','menu_name','portion_name','rate','GST','Barcode',
];
   public function manu(){
        return $this->hasOne('App\Models\Menu','menu_id','menu_id');
    }
}
