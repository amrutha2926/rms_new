<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kitchen extends Model
{
    protected $primaryKey = 'kitchen_id';
  protected $fillable = [
      'kitchen_type', 'status',
  ];
}
