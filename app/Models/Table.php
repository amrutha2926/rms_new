<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
  protected $primaryKey = 'table_id';
protected $fillable = [
    'table_no','seats', 'status',
  ];
  public function seats(){
    return $this->hasMany('App\Models\Seat','table_id','table_id');
  }
}
