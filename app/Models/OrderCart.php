<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderCart extends Model
{
    protected $tabale = 'order_carts';
    protected $primaryKey = 'order_id';
    protected $fillable = ['rate_id','table_id', 'order_status'];
    
     public function manurate(){
        return $this->hasMany('App\Models\Menurate','rate_id','rate_id');
    }
}
