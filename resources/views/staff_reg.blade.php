<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Menu Master</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<link href="{{asset('app_assets/css/status.css')}}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{asset('app_assets/css/base.css')}}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{asset('app_assets/css/header.css')}}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{asset('app_assets/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<link rel="stylesheet" href="{{asset('app_assets/css/jquery.mcustomscrollbar.css')}}" />
<script type="text/javascript" src="{{asset('app_assets/js/style.js')}}"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript">
  $(function() {
    $(".js__p_start, .js__p_another_start").simplePopup();
  });
</script>
</head>

<body>
  <!--container starts-->
<div class="container">
    <div class="containerData">
        <div class="statusMain">
            <div class="leftSection leftSec">
                 <div class="sidebar-menu leftSec">
                      <header class="logoOne leftSec">
                        <a href="#" class="sidebar-icon leftSec"> <span class="leftMenu leftSec"></span> </a>
                      </header>
                      <div class="menu leftSec">
                          <ul id="menu" class="menuList leftSec">

                              <li class="menuListOne leftSec"><a href="#" class="menuOne leftSec active"><i class="leftSec"><img class="leftSec" src="{{asset('app_assets/images/leftmenutwo.png')}}"></i><span class="leftSec leftText">Demo</span><span class="menuIcon leftSec"></span></a>
                                  <ul class="leftSec">
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                  </ul>
                              </li>
                              <li class="menuListOne leftSec"><a href="#" class="menuOne leftSec"><i class="leftSec"><img class="leftSec" src="{{asset('app_assets/images/leftmenuthree.png')}}"></i><span class="leftSec leftText">Demo</span><span class="menuIcon leftSec"></span></a>
                                <ul class="leftSec">
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                  </ul>
                              </li>
                              <li class="menuListOne leftSec"><a href="#" class="menuOne leftSec"><i class="leftSec"><img class="leftSec" src="{{asset('app_assets/images/leftmenufour.png')}}"></i><span class="leftSec leftText">Demo</span><span class="menuIcon leftSec"></span></a>
                                <ul class="leftSec">
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                  </ul>
                              </li>

                              <li class="menuListOne leftSec"><a href="#" class="menuOne leftSec"><i class="leftSec"><img class="leftSec" src="{{asset('app_assets/images/leftmenufive.png')}}"></i><span class="leftSec leftText">Demo</span><span class="menuIcon leftSec"></span></a>
                                <ul class="leftSec">
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                  </ul>
                              </li>
                              <li class="menuListOne leftSec"><a href="#" class="menuOne leftSec"><i class="leftSec"><img class="leftSec" src="{{asset('app_assets/images/leftmenusix.png')}}"></i><span class="leftSec leftText">Demo</span><span class="menuIcon leftSec"></span></a>
                                <ul class="leftSec">
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                  </ul>
                              </li>
                              <li class="menuListOne leftSec"><a href="#" class="menuOne leftSec"><i class="leftSec"><img class="leftSec" src="{{asset('app_assets/images/leftmenusevn.png')}}"></i><span class="leftSec leftText">Demo</span><span class="menuIcon leftSec"></span></a>
                                <ul class="leftSec">
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                      <li><a href="#" class="leftSec">Demo</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </div>
            </div>
              </div>

          </div>
      </div>
  </div>
    <div class="p_body js__p_body js__fadeout"></div>
    <div class="popup js__popup js__slide_top"> <a href="#" class="p_close js__p_close" title="Close"></a>
        <div class="p_content">
          <h1>Add staff</h1>
          <div class="fl">
            <form action="/staff_reg" method="POST" enctype="multipart/form-data">
            @csrf

            <ul>
              <li>
              <label>Name<span>*</span></label>
            <input type="text" name="name" id="textfield"  placeholder="Name">
          </li>
            <li>
              <label>Email <span>*</span></label>
                <input type="text" name="email" id="textfield" placeholder="Email">
              </li>
              <li class="txtarea">
                  <label>Address</label>
                  <textarea name="address" id="textarea" cols="35" rows="3" placeholder="Address"></textarea>
                </li>
                <li>
                  <label>Salary <span>*</span></label>
                    <input type="text" name="salary" id="textfield" placeholder="salary">
                  </li>
                  <li>
                    <label>Photo <span>*</span></label>
                       <input type="file" name="image" id="textfield" placeholder="">
                    </li>
                    <li>
                      <label>Staff Type <span>*</span></label>
                        <input type="text" name="staff_type" id="textfield" placeholder="Staff Type">
                      </li>
                  <li>
                    <label>Authentication Key <span>*</span></label>
                      <input type="text" name="a_key" id="textfield" placeholder="Authentication key">
                    </li>
                    <li>
                      <label>Password <span>*</span></label>
                        <input type="text" name="password" id="textfield" placeholder="Password">
                      </li>
                      <li>
                        <label>Re-type Password <span>*</span></label>
                          <input type="text" name="r_password" id="textfield" placeholder="Re-type Password">
                        </li>
          </ul>
          <div class="btn-cr">
          <input type="submit" name="button" id="button" value="Save & Proceed" class="btn-b"></div>

    </div>
          </form>
        </div>

      </div>

<!--header starts-->
<section class="top-txt">Staff Management</section>
<!--header ends-->
<!--container starts-->
<div class="container">
  <div class="containerStatus">
    <div class="statusMain1">
        <div class="fr">
            <a href="#" class="js__p_start">Add Staff +</a>
            <!-- <button class="fl last" title="Add Menu">Add Menu +</button> -->
            </div>
      <div class="statusMainRight">

        <div class="statusBottom">

          <table>
            <thead>
              <tr>
                <th>Photo</th>
                <th>Name</th>
                <th>Address</th>
                <th>Staff Type</th>
                 <th>Salary</th>
                  <th>Authentication Key</th>
                 <th>Password</th>
                 <th>Update</th>

              </tr>
            </thead>
            <tbody>
              @foreach (c as $user)
        <tr>
           <td><img src="{{asset('file/'.$user->photo)}}" height="50px" width="50px"/></td>
           <td>{{ $user->name }}</td>
           <td>{{ $user->address }}</td>
           <td>{{ $user->Staff_type }}</td>
           <td>{{ $user->salary }}</td>
           <td>{{ $user->A_key }}</td>
           <td>{{ $user->password }}</td>

           <td><div class="img-r"><a href= 'edit/{{ $user->id }}' title="Edit"><img src="{{asset('app_assets/images/edit.png')}}"></a></div><div class="img-r"><a href='destroy/{{ $user->id }}' title="Delete"><img src="{{asset('app_assets/images/delete.png')}}"></a></div></td>


              @endforeach
              </tr>
            </tbody>
          </table>
          <div class="clearfloat pagination">
            <div class="fl paginationLeft">
              <h1 class="fl">Select Page</h1>
              <div class="fl niceTwo">
                <div class="nice-select fl" name="nice-select">
                  <input type="text" value="" readonly placeholder="10">
                  <ul>
                    <li data-value="1">1</li>
                    <li data-value="2">2</li>
                    <li data-value="3">3</li>
                  </ul>
                </div>

              </div>

            </div>
            <ul class="fr paginationRight">
              <li><span><img src="{{asset('app_assets/images/statusleft.png')}}"></span>Prev</li>
              <li class="active">1</li>
              <li>2</li>
              <li>3</li>
              <li>4</li>
              <li>5</li>
              <li>6</li>
              <li>7</li>
              <li>8</li>
              <li>...</li>
              <li>20</li>
              <li>Nex<span><img src="{{asset('app_assets/images/statusright.png')}}"></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>
<!--container ends-->
<!--footer starts-->
<!--footer ends-->

<!--scripts starts-->
<script src="{{asset('app_assets/js/jquery-2.1.4.js')}}"></script>
<script src="{{asset('app_assets/js/pulldown.js')}}"></script>
<script src="{{asset('app_assets/js/style.js')}}"></script>
<script src="{{asset('app_assets/js/sidebar.js')}}"></script>
<script src="{{asset('app_assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{asset('app_assets/js/status.js')}}"></script>
<script src="{{asset('app_assets/js/jquery.placeholder.js')}}"></script>
<script>
         $(function() {
          $('input, textarea').placeholder({customClass:'my-placeholder'});
          var html;

          if ($.fn.placeholder.input && $.fn.placeholder.textarea) {

          } else if ($.fn.placeholder.input) {

          }

          if (html) {
           $('<p class="note">' + html + '</p>').insertBefore('form');
          }
         });
      </script>
</body>
</html>
