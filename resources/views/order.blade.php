<html>
<head>
<title>Order</title>
    <link rel="stylesheet" type="text/css" href="{{asset('Managers/css/style.css')}}">
    <link href="{{asset('Managers/css/base.css')}}" rel="stylesheet" type="text/css" media="all"/>
<body>
  <section class="top-head">
      <div class="wid">
      <a href="" class="log-top">Logo</a>
      <div class="fr">
          <ul>
              <li><a href="" title="Home"><img src="{{asset('Managers/images/home.png')}}"></a></li>
              <li><a href="" title="Back"><img src="{{asset('Managers/images/reply.png')}}"></a></li>
              <li><a href="" title="Settings"><img src="{{asset('Managers/images/settings-gears.png')}}"></a></li>
          </ul>
      </div>
      </div>

</section>
<section class="full-cvr">
    <div class="wid">
        <ul>
            <li>
                <div class="img-c">
                <a href="{{route('order.show')}}" title="New Order"><img src="{{asset('Managers/images/add-file.png')}}"></a>
            </div>
            <label>NEW ORDER</label>
            </li>
            <li>
                    <div class="img-c">
                <a href="" title="Order History"><img src="{{asset('Managers/images/refresh.png')}}"></a>
                </div>
                <label>ORDER HISTORY</label>
                 </li>

        </ul>
    </div>
</section>
</body>
</head>
</html>
