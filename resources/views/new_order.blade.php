<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="{{asset('order/css/style.css')}}" />
<title>Display</title>
<style>
a:focus
{
    background:green;
}
</style>
</head>

<body>
        <section class="top-headd">
                <div class="wid">
                <a href="" class="log-top">Logo</a>
                <div class="fr">
                    <ul>
                        <li><a href="" title="Home"><img src="{{asset('order/images/home.png')}}"></a></li>
                        <li><a href="" title="Back"><img src="{{asset('order/images/reply.png')}}"></a></li>
                        <li><a href="" title="Settings"><img src="{{asset('order/images/settings-gears.png')}}"></a></li>
                    </ul>
                </div>
                </div>

        </section>
<section class="all-one">
<div class="wid">
<div class="order-type_left">
<ul class="orderUlF1">
<li><a href="#"  class="active dine"><span>Dine In</span></a></li>
<li><a href="#" class="o_dine"><span>Delivery</span></a></li>
<li><a href="#" class="o_dine"><span>Swiggy</span></a></li>
<li><a href="#" class="o_dine"><span>Zomato</span></a></li>
<li><a href="#" class="o_dine"><span>Take Away</span></a></li>
<li><a href="#"  class="o_dine"><span>Order Type</span></a></li>
</ul>

<ul class="orderUlF2 tbl_count"  style="display:none">
<h2>table No</h2>
@foreach($tables as $table)
{
<li><a class="persons" data-nop="{{ $table->table_id}}">{{$table->table_no}}</a></li>
}
@endforeach
</ul>

<ul class="orderUlF2 tbl_count"  style="display:none">
<h2>No Of Persons</h2>
@foreach($tables as $table)
{
<li><a href="#">{{ $table->table_no}}</a></li>
}
@endforeach
</ul>
</div>
<div class="order-type_center">
<h2><label id="tbl_no"></label> <a id="new_order" href="#">NEW ORDER <span>+</span> </a> <label id="tbl_no"></label></h2>
<input type="hidden"  id="hid_table_no">
<table class="tableClCtr" id="menurate" width="100%">
        <input type="text" id="search_order" placeholder="Search"> <div id="result-box"></div>
<tbody>
    
</tbody>

<tfoot>
<tr>
<td colspan="2" width="50%"><input type="button" id="confirm_order"  value="Confirm" class="ftbtnsx"></td>
<td colspan="2" width="50%"><input type="button"  value="Print Bill" class="ftbtnsx"/></td>
</tr>
<tr>
<td width="50%"><input type="button"  value="Settle Bill" class="ftbtnsx"/></td>
<td width="50%"><input type="button"  value="Customer Details" class="ftbtnsx"/></td>
</tr>
<tr>
<td width="50%"><input type="button"  value="Cancel Bill" class="ftbtnsx"/></td>
<td width="50%"><input type="button"  value="Reprint Bill" class="ftbtnsx"/></td>
</tr>

</tfoot>
</table>
</div>
<div class="order-type_right">
<h2>Bill</h2>
<ul class="kotno">
<li><a href="#">Kot No 1</a></li>
<li><a href="#">Kot No 2</a></li>
</ul>

<table class="tableCl1" id="tblBill" width="100%">
<tbody>
 
</tbody>
<tfoot>
<tr>
<td colspan="2" width="50%">TOTAL</td>
<td colspan="2" width="50%" align="right" style="font-size:18px">AED <strong>53.5</strong></td>
</tr>
<tr>
<td width="70%"><span>(%) <input type="text" /></span>
<span>(AED) <input type="text" /></span>
</td>
<td width="30%" align="right"><input type="submit"  value="submit"/></td>
</tr>
<tr>
<td colspan="2" width="50%">GRAND TOTAL</td>
<td colspan="2" width="50%" align="right" style="font-size:18px">AED <strong>53.5</strong></td>
</tr>
</tfoot>
</table>

</div>
</div>
</section>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function () {
    $(".persons").click(function () 
    {
        
        var per = $(this).data("nop");
        check_order_exist(per);        
        $("#tbl_no").html(per);
        $("#hid_table_no").val(per);

    });
});
$(document).ready(function () {
    $(".dine").click(function () {
        $(".tbl_count").show();
    });
     
});


$(document).ready(function () {
$(".o_dine").click(function () {
    $(".tbl_count").hide();
    });
});
</script>

<script type="text/javascript">
    $(function(){
      $("#new_order").click(function () {
        $("#search_order").show();
      });
    });

    $(document).ready(function(){
	$("#search_order").keyup(function(){         
		$.ajax({
		type: "POST",
		url: "{{route('order.get_products')}}",
		data:{keyword: $(this).val(), _token: '{{csrf_token()}}'},
		success: function(data){
            
			$("#result-box").show();
			$("#result-box").html(data);
			// $("#search_order").css("background","#FFF");
		}
		});
	});
});

$(document).on('click', '.menu_item', function(e){  

        var productName = $(this).text();

        $('#search_order').val($(this).text()); 
        var menu_id = $(this).attr('id');
        var table_no = $("#hid_table_no").val();
        $("#result-box").hide();

        $.ajax({
		type: "POST",
		url: "{{route('order.get_menu_items')}}",
		data:{menu_value: menu_id, table: table_no, _token: '{{csrf_token()}}'},
		success: function(data)
        {
            $('#menurate').append(data);	 
		}
        });
        
        e.preventDefault();
    e.stopPropagation();
       
    });  

    var counter=0;
    $(document).on('click', '#confirm_order', function()
    {
        var table_no = $("#hid_table_no").val();

        $.ajax({
		type: "POST",
		url: "{{route('order.confirm_order')}}",
		data:{tableno: table_no, _token: '{{csrf_token()}}'},
		success: function(data)
        {
            if(counter<=0)
            {
                $('#tblBill').append(data);	                 

                counter++;
            }
		}
		});
       

        // if(counter<=0)
        // {
        //     var a = $("#menurate").find(">tbody").each(function () {
        //         $('#tblBill tbody').append($(this).html());
                
                
        // });
        
        // counter++;
        // }
    
    });  
      function check_order_exist(per){
        $.ajax({
		type: "get",
		url: "{{route('order.check.exist')}}",
		data:{id:per},
		success: function(data)
        {
          $('#menurate tbody').html(data);
		}
		});
      }    

</script>

</body>
</html>
