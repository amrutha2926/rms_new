<!--
 // **************************************************
 // ******* Name: drora
 // ******* Description: Bootstrap 4 Admin Dashboard
 // ******* Version: 1.0.0
 // ******* Released on 2019-02-08 15:41:24
 // ******* Support Email : quixlab.com@gmail.com
 // ******* Support Skype : sporsho9
 // ******* Author: Quixlab
 // ******* URL: https://quixlab.com
 // ******* Themeforest Profile : https://themeforest.net/user/quixlab
 // ******* License: ISC
 // ***************************************************
-->

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.themefisher.com/drora/main/template/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Dec 2019 17:32:56 GMT -->


<body>

    <!--*******************
        Preloader start
    ********************-->

    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="index.html">
                    <b class="logo-abbr">R</b>
                    <span class="brand-title"><b>RMS</b></span>
                </a>
            </div>
            <div class="nav-control">
                <div class="hamburger">
                    <span class="toggle-icon"><i class="icon-menu"></i></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->
@include('header')

        <div class="content-body">


            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-between mb-3">
					<div class="col-12 text-left">
						<h2 class="page-heading">Hi,Welcome Back!</h2>

					</div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h3 class="content-heading">FOOD CATEGORY </h3>
                    </div>


                    <div class="col-xl-6 col-xxl-12">
                        <div class="card ">
                            <div class="card-body">
                                <h4 class="card-title">Food Category</h4>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong" style="float:Right;margin-bottom: 10px;">Food Category +</button>
                                <!-- Modal -->
                                <div class="modal fade " id="exampleModalLong" >
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Add Food Category</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body ">
                                                <div class="basic-form">
                                                    <form action="{{route('category.add')}}" method="post">
                                                               @csrf
                                                        <div class="form-row ">
                                                            <div class="form-group col-md-12">
                                                                <label>Category Name</label>
                                                                <input type="text" class="form-control"  name="category_name" placeholder="Category Name">
                                                            </div>
                                                          </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <input type="submit" class="btn btn-primary" class="btn btn-primary">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- Button trigger modal -->
                                <!-- Modal2-->
                                <div class="modal fade " id="exampleModalLong1" >
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Category </h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body ">
                                                <div class="basic-form">
                                                  <form action ="{{route('category.edit')}}" method = "post">
                                                 <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                                        <div class="form-row ">
                                                          <div class="form-group col-md-12">
                                                              <label>Id</label>
                                                              <input type="text" class="form-control" id="f_id" name="category_id" placeholder="Menu Name" readonly>
                                                          </div>
                                                            <div class="form-group col-md-12">
                                                                <label>Category Name</label>
                                                                <input type="text" class="form-control" id="f_cat" name="category_name" placeholder="Category Name">
                                                            </div>
                                                          </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <input type="submit" class="btn btn-primary" class="btn btn-primary">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- Button trigger modal -->
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped verticle-middle table-responsive-sm">
                                        <thead>
                                            <tr>
                                              <th scope="col">No</th>
                                                <th scope="col">Id</th>
                                                <th scope="col">Category Name</th>

                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                      <?php   $i=0; ?>
                                        <tbody id="users-crud">
                                    @foreach ($categories as $category)
                                    <tr>
                                       <td>{{ ++$i }}</td>
                                       <td>{{ $category->category_id }}</td>
                                       <td>{{ $category->category_name  }}</td>

                                                <td><span><button class="mr-4 open-modal"  data-placement="top" title="Edit"id="edit-user" data-id="{{ $category->category_id }}"
 data-f_cat="{{ $category->category_name }}" data-toggle="modal" data-target="#exampleModalLong1"><i
                                                                class="fa fa-pencil color-muted"></i> </button><a href="{{route('category.destroy',['id'=>$category->category_id])}}" onclick="return confirm('Are you sure you want to delete this item?');"
                                                            data-toggle="tooltip" data-placement="top" title="Delete"><i
                                                                class="fa fa-close color-danger"></i></a></span>
                                                </td>
                                            </tr>
                                              @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed & Developed by <a href="" target="_blank">otlet.in</a> 2019</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->



    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="{{asset('app_assets/assets/plugins/common/common.min.js')}}"></script>
    <script src="{{asset('app_assets/js/custom.min.js')}}"></script>
    <script src="{{asset('app_assets/js/settings.js')}}"></script>
    <script src="{{asset('app_assets/js/quixnav.js')}}"></script>
    <script src="{{asset('app_assets/js/styleSwitcher.js')}}"></script>

    <!-- JS Grid -->
    <script src="{{asset('app_assets/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('app_assets/assets/plugins/jsgrid/js/jsgrid.min.js')}}"></script>
    <!-- Footable -->
    <script src="{{asset('app_assets/assets/plugins/footable/js/footable.min.js')}}"></script>
    <!-- Bootgrid -->
    <script src="{{asset('app_assets/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js')}}"></script>
    <!-- Datatable -->
    <script src="{{asset('app_assets/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>


    <!-- JS Grid Init -->
    <script src="{{asset('app_assets/js/plugins-init/jsgrid-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/footable-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/jquery.bootgrid-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/datatables.init.js')}}"></script>

    <script>
    $(document).ready(function () {
    $('button.open-modal').on('click', function (e) {
        // Make sure the click of the button doesn't perform any action
        e.preventDefault();

        // Get the modal by ID
        var modal = $('#exampleModalLong1');

        // Set the value of the input fields
        modal.find('#f_cat').val($(this).data('f_cat'));

        modal.find('#f_id').val($(this).data('id'));

        // Update the action of the form
      //s  modal.find('form').attr('action', 'tasks/update/' + $(this).val());

    });
});

    </script>

</body>


<!-- Mirrored from demo.themefisher.com/drora/main/template/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Dec 2019 17:32:58 GMT -->
</html>
