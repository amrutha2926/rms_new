<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    //return view('login');
//});
Route::get('/', function () {
    return view('welcome');
});

//menu
Route::get('menu','MenuaddController@index')->name('menu.index');
Route::post('menuadd','MenuaddController@add')->name('menu.add');
Route::get('menumaster','MenuController@show')->name('menumaster.show');
Route::get('billing','BillController@show')->name('billing.show');
Route::get('new-order','OrderController@index')->name('order.index');
Route::post('get_products','OrderController@get_products')->name('order.get_products');
Route::post('get_menu_items','OrderController@get_menu_items')->name('order.get_menu_items');
Route::post('confirm_order','OrderController@place_order')->name('order.confirm_order');

//Route::get('search/{id}','OrderController')->name('order.search');
//Menurate
Route::get('/menurate','MenurateController@index')->name('menurate.index');
//kitchen//
Route::post('/add_k', ['uses' =>'kitchenController@add'])->name('kitchen.add');
Route::get('/Kitchen','kitchenController@show')->name('kitchen');
Route::post('edit_k','kitchenController@edit')->name('kitchen.edit');
Route::get('destroy_k/{id}','kitchenController@destroy')->name('kitchen.destroy');
//Category//
Route::post('/add',['uses' =>'food_categoryController@add'])->name('category.add');
Route::get('/Category','food_categoryController@show')->name('Category');
Route::post('edit','food_categoryController@edit')->name('category.edit');
Route::get('destroy_cat/{id}','food_categoryController@destroy')->name('category.destroy');
//portion//
Route::post('/add_p',['uses' =>'portionController@add'])->name('portion.add');
Route::get('/Portion','portionController@show')->name('portion.show');
Route::post('edit_p','portionController@edit')->name('portion.edit');
Route::get('destroy_p/{id}','portionController@destroy')->name('portion.destroy');
//currency//
Route::post('/add_cur',['uses' =>'CurrencyController@add'])->name('currency.add');
Route::get('/Currency','CurrencyController@show')->name('currency');
Route::post('/edit_cur','CurrencyController@edit')->name('currency.edit');
Route::get('destroy_c/{id}','CurrencyController@destroy')->name('currency.destroy');
//tables//
Route::post('/add_tbl',['uses' =>'tableController@add'])->name('table.add');
Route::get('/Table','tableController@show')->name('table.show');
Route::post('/edit_tbl','tableController@edit')->name('table.edit');
Route::get('destroy_tbl/{id}','tableController@destroy')->name('table.destroy');
//mode//
Route::post('/add_m',['uses'=>'ModeController@add'])->name('mode.add');
Route::get('/Mode','ModeController@show')->name('mode');
Route::post('/edit_m','ModeController@edit')->name('mode.edit');
Route::get('destroy_m/{id}','ModeController@destroy')->name('mode.destroy');
Route::get('table-seat/{id}','OrderController@tables_seat')->name('table.seat');
Route::get('order-show','OrderController@index')->name('order.show');
Route::get('check_order_exist','OrderController@check_order_exist')->name('order.check.exist');



//Route::get('image-upload', 'staff_reg_Controller@staff_reg');
//Route::post('image-upload', 'staff_reg_Controller@imageUploadPost')->name('image.upload.post');
